from django.shortcuts import redirect, render
from lists.models import Money


def home_page(request):
    if request.method == 'POST':
        money = request.POST['money']
        Money.objects.create(moneytoday=money)
        return redirect('/')

    items = Money.objects.all()
    listmoney = []
    avmoney = 0.00
    summoney = 0
    count = 0
    Min = 0
    Max = 0
    for i in items:
        summoney = summoney+i.moneytoday
        listmoney.append(i.moneytoday)
        count = count+1

    mintomax = sorted(listmoney)
    maxtomin = sorted(listmoney, reverse=True)

    if count != 0:
        avmoney = summoney/count
        Min = mintomax[0]
        Max = maxtomin[0]

    return render(request, 'home.html', {'allmoney': items,
                                         'summoney': summoney,
                                         'avmoney': avmoney,
                                         'mintomax': mintomax,
                                         'maxtomin': maxtomin,
                                         'min': Min,
                                         'max': Max, }
                  )
