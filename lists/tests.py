from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase

from lists.models import Money
from lists.views import home_page


class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        # self.assertEqual(response.content.decode(), expected_html)

    def test_home_page_displays_all_list_items(self):
        Money.objects.create(moneytoday=100)
        Money.objects.create(moneytoday=200)

        request = HttpRequest()
        response = home_page(request)

        self.assertIn(
            'ผลรวมเงินออม : 300 บาท เฉลี่ยเงินออมต่อครั้ง : 150.0 บาท'
            ' ฝากต่ำสุด :100 บาท ฝากสูงสุด : 200 บาท',
            response.content.decode())
        self.assertIn('ออมครั้งที่ 1: 100 บาท', response.content.decode())
        self.assertIn('ออมครั้งที่ 2: 200 บาท', response.content.decode())

    def test_home_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['money'] = '100'

        response = home_page(request)

        self.assertEqual(Money.objects.count(), 1)
        new_money = Money.objects.first()
        self.assertEqual(new_money.moneytoday, 100)

    def test_home_page_redirects_after_POST(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['money'] = '100'

        response = home_page(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_home_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        home_page(request)
        self.assertEqual(Money.objects.count(), 0)


class MoneyModelTest(TestCase):

    def test_saving_and_retrieving_items(self):
        first_money = Money()
        first_money.moneytoday = 400
        first_money.save()

        second_money = Money()
        second_money.moneytoday = 600
        second_money.save()

        third_money = Money()
        third_money.moneytoday = 500
        third_money.save()

        saved_money = Money.objects.all()
        self.assertEqual(saved_money.count(), 3)

        first_saved_money = saved_money[0]
        second_saved_money = saved_money[1]
        third_saved_money = saved_money[2]
        self.assertEqual(first_saved_money.moneytoday, 400)
        self.assertEqual(second_saved_money.moneytoday, 600)
        self.assertEqual(third_saved_money.moneytoday, 500)
