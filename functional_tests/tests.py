from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_money(self, row_text):
        table = self.browser.find_element_by_id('id_list_table_money')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def check_for_row_in_list_allmoney(self, row_text):
        table = self.browser.find_element_by_id('id_list_table_allmoney')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get(self.live_server_url)

        self.assertIn('app เก็บเงินออม', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('การหยอดเงินออม', header_text)

        inputbox = self.browser.find_element_by_id('id_new_money')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a money'
        )

        inputbox.send_keys('100')
        inputbox.send_keys(Keys.ENTER)
        self.check_for_row_in_list_money(
            'เรียงตามเวลา\n\nออมครั้งที่ 1: 100 บาท\nเรียงจากน้อยไปมาก'
            '\n\n100 บาท\nเรียงจากมากไปน้อย\n\n100 บาท')


        inputbox = self.browser.find_element_by_id('id_new_money')
        inputbox.send_keys('200')
        inputbox.send_keys(Keys.ENTER)

        self.check_for_row_in_list_money(
            'เรียงตามเวลา\n\nออมครั้งที่ 1: 100 บาท\n\nออมครั้งที่ 2: 200 บาท'
            '\nเรียงจากน้อยไปมาก\n\n100 บาท\n\n200 บาท\nเรียงจากมากไปน้อย'
            '\n\n200 บาท\n\n100 บาท')

        self.check_for_row_in_list_allmoney(
            'ผลรวมเงินออม : 300 บาท เฉลี่ยเงินออมต่อครั้ง : 150.0 บาท '
            'ฝากต่ำสุด :100 บาท ฝากสูงสุด : 200 บาท')

        self.fail('Finish the test!')
